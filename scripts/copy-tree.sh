#!/bin/bash

# Use the tree command with the -I flag to exclude certain folders
tree -L 2 -a -I 'node_modules|build|__pycache__|temp_tree.txt|.DS_Store|.idea|.venv' > temp_tree.txt

# Copy the resulting output to the clipboard
cat temp_tree.txt | pbcopy

# Delete the temporary file
rm temp_tree.txt