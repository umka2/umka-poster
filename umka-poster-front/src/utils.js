export function formatTime(seconds) {
    if (seconds < 60) {
        return `${seconds.toFixed(2)} секунд`;
    } else {
        return `${(seconds / 60).toFixed(2)} минут`;
    }
}