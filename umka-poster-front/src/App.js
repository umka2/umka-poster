import React, { useState } from 'react';
import './App.css';
import AdminPanel from './components/AdminPanel/AdminPanel';
import LoginForm from './components/LoginForm/LoginForm';
import { EventProvider } from './EventContext';


function App() {
  const [user, setUser] = useState(null);
  const [isLoggedIn, setIsLoggedIn] = useState(false);
  const [events, setEvents] = useState([]);


  const handleLogin = (authenticatedUser) => {
    console.log('User authenticated successfully:', authenticatedUser);
    setIsLoggedIn(true);
    setUser(authenticatedUser);
  };

  const handleLogout = () => {
    console.log('User logout:', user);
    setIsLoggedIn(false);
    setUser(null);
  };  

  return (
    <EventProvider>
      <div className="App">
        {isLoggedIn ? <AdminPanel user={user} onLogout={handleLogout} /> : <LoginForm onLogin={handleLogin} />}
      </div>
    </EventProvider>
  );
}

export default App;
