import React, { useEffect, useState } from 'react';
import Pagination from 'react-bootstrap/Pagination';
import { fetchAPI } from '../../api';


const ITEMS_PER_PAGE = 50; // Количество элементов на странице

const StatsTab = ({ isActive }) => {
    const [currentStatsPageData, setCurrentStatsPageData] = useState([]);
    const [currentPage, setCurrentPage] = useState(1);
    const [totalPages, setTotalPages] = useState(1);

    useEffect(() => {
        if (isActive) {
            fetchStatsData(currentPage);
        }
    }, [isActive, currentPage]);


    const formatLocalDate = (timestamp) => {
        const date = new Date(timestamp);
        
        // Корректировка для вашего часового пояса
        const offset = date.getTimezoneOffset();
        date.setMinutes(date.getMinutes() - offset);
    
        const day = String(date.getDate()).padStart(2, '0');
        const month = String(date.getMonth() + 1).padStart(2, '0');
        const year = date.getFullYear();
    
        const hours = String(date.getHours()).padStart(2, '0');
        const minutes = String(date.getMinutes()).padStart(2, '0');
        const seconds = String(date.getSeconds()).padStart(2, '0');
    
        return `${year}-${month}-${day} ${hours}:${minutes}:${seconds}`;
    }

    const fetchStatsData = async (page) => {
        try {
            console.log(`Fetching stats data for page: ${page}`);
            
            const response = await fetchAPI(`/get_broadcast_statistics?page=${page}&items_per_page=${ITEMS_PER_PAGE}`);
            
            if (!response.ok) {
                throw new Error(`Server responded with status: ${response.status}`);
            }
            
            const data = await response.json();
            console.log("Received data from server:", data);
    
            if (!data || !Array.isArray(data.items)) {
                console.warn("Expected data.items to be an array but received:", data.items);
                setCurrentStatsPageData([]);
            } else {
                setCurrentStatsPageData(data.items);
            }
    
            if (data.totalRecords && typeof data.totalRecords === 'number') {
                setTotalPages(Math.ceil(data.totalRecords / ITEMS_PER_PAGE));
            } else {
                console.warn("Expected data.totalRecords to be a number but received:", data.totalRecords);
            }
    
        } catch (error) {
            console.error("Error fetching stats data:", error);
        }
    };
    
    const handlePageChange = (pageNumber) => {
        setCurrentPage(pageNumber);
    };

    const mapBroadcastType = (type) => {
        return type === "test" ? "Тест" : "Подписчики";
    }

    return (
        <div className={`tab-pane ${isActive ? 'active' : ''}`} id="stats">
            <table className="table">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>Время</th>
                        <th>Текст сообщения</th>
                        <th>Изображение</th>
                        <th>Успешно</th>
                        <th>Отписки</th>
                        <th>Ошибки</th>
                        <th>Продолжительность</th>
                        <th>Тип рассылки</th>
                    </tr>
                </thead>
                <tbody>
                    {Array.isArray(currentStatsPageData) && currentStatsPageData.map((stat, index) => (
                        <tr key={stat.timestamp}>
                            <td>{(currentPage - 1) * ITEMS_PER_PAGE + (index + 1)}</td>
                            <td>{formatLocalDate(stat.timestamp)}</td>
                            <td className="truncate">{stat.message_text}</td>
                            <td className="truncate">{stat.image_id}</td>
                            <td>{stat.success_count}</td>
                            <td>{stat.unsubscribed_count}</td>
                            <td>{stat.failure_count}</td>
                            <td>{parseFloat(stat.elapsed_time).toFixed(2)} сек</td>
                            <td>{mapBroadcastType(stat.broadcast_type)}</td>
                        </tr>
                    ))}
                </tbody>
            </table>
            <div className='pagination-right mt-3'>
                <Pagination>
                    <Pagination.First onClick={() => handlePageChange(1)} />
                    <Pagination.Prev onClick={() => handlePageChange(currentPage - 1)} disabled={currentPage === 1} />
                    {[...Array(totalPages).keys()].map(page => (
                        <Pagination.Item key={page + 1} active={page + 1 === currentPage} onClick={() => handlePageChange(page + 1)}>
                            {page + 1}
                        </Pagination.Item>
                    ))}
                    <Pagination.Next onClick={() => handlePageChange(currentPage + 1)} disabled={currentPage === totalPages} />
                    <Pagination.Last onClick={() => handlePageChange(totalPages)} />
                </Pagination>
            </div>
        </div>
    );
}

export default StatsTab;
