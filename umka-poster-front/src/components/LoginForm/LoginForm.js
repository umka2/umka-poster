import React, { useState } from 'react';
import { fetchAPI } from '../../api';


const LoginForm = ({ onLogin }) => {
    const [username, setUsername] = useState('');
    const [password, setPassword] = useState('');
    const [errorMessage, setErrorMessage] = useState('');

    const handleSubmit = async (e) => {
      e.preventDefault();
      console.log('Attempting to authenticate user:', username);
      if (username && password) {
          try {
              const response = await fetchAPI('/authenticate', {
                  method: 'POST',
                  headers: {
                      'Content-Type': 'application/json'
                  },
                  body: JSON.stringify({ login: username, password })
              });
  
              const data = await response.json();
              console.log('Server response:', data);
  
              if (response.status === 200) {
                  onLogin(data.user);
              } else {
                  setErrorMessage(data.message || 'Authentication failed');
              }
          } catch (error) {
              setErrorMessage('An error occurred. Please try again.');
              console.error('Error during authentication:', error);
          }
      }
  };
  


    return (
        <div className="container mt-5">
          <div className="row justify-content-center">
            <div className="col-md-4">
              <div className="card">
                <div className="card-body">
                  <form onSubmit={handleSubmit}>
                    <div className="form-group">
                      <label htmlFor="username">Логин</label>
                      <input
                        type="text"
                        className="form-control"
                        id="username"
                        placeholder="Введите логин"
                        value={username}
                        onChange={(e) => setUsername(e.target.value)}
                        autocomplete="username"
                      />
                    </div>
                    <div className="form-group">
                      <label htmlFor="password">Пароль</label>
                      <input
                        type="password"
                        className="form-control"
                        id="password"
                        placeholder="Введите пароль"
                        value={password}
                        onChange={(e) => setPassword(e.target.value)}
                        autocomplete="current-password"
                      />
                    </div>
                    <button type="submit" className="btn btn-primary">Войти</button>
                  </form>
                  {errorMessage && <p className="text-danger mt-3">{errorMessage}</p>}
                </div>
              </div>
            </div>
          </div>
        </div>
    );
}

export default LoginForm;
