import React, { useState } from 'react';
import BroadcastTab from '../BroadcastTab/BroadcastTab';
import JournalTab from '../JournalTab/JournalTab';
import StatsTab from '../StatsTab/StatsTab';
import SettingsTab from '../SettingsTab/SettingsTab';


const AdminPanel = ({user, onLogout}) => {
    const [activeTab, setActiveTab] = useState('settings');  // начальное состояние для активной вкладки
    const [statistics, setStatistics] = useState(null); // начальное состояние для статистики (результатов) рассылки


    console.log("AdminPanel render. Active tab:", activeTab); // логирование текущей активной вкладки


    return (
        <div className="container mt-5">
            <header className="bg-primary text-white text-center py-3">
                <h1>Публикатор</h1>
                <p>Добро пожаловать, {user.user_login}</p>
                <button onClick={onLogout} className="btn btn-primary">
                    Выйти
                </button>
            </header>

            <ul className="nav nav-tabs">
                <li className="nav-item">
                    <a className={`nav-link ${activeTab === 'settings' ? 'active' : ''}`} href="#settings" onClick={() => setActiveTab('settings')}>Настройки</a>
                </li>
                <li className="nav-item">
                    <a className={`nav-link ${activeTab === 'broadcast' ? 'active' : ''}`} href="#broadcast" onClick={() => setActiveTab('broadcast')}>Рассылка</a>
                </li>
                <li className="nav-item">
                    <a className={`nav-link ${activeTab === 'journal' ? 'active' : ''}`} href="#journal" onClick={() => setActiveTab('journal')}>Журнал</a>
                </li>
                <li className="nav-item">
                    <a className={`nav-link ${activeTab === 'stats' ? 'active' : ''}`} href="#stats" onClick={() => setActiveTab('stats')}>Статистика</a>
                </li>
            </ul>

            <div className="tab-content mt-3">
                <SettingsTab isActive={activeTab === 'settings'} user={user} />
                <BroadcastTab isActive={activeTab === 'broadcast'} setStatistics={setStatistics} />
                <JournalTab isActive={activeTab === 'journal'} statistics={statistics} />
                <StatsTab isActive={activeTab === 'stats'} />
            </div>

            <footer className="bg-dark text-white text-center py-3 mt-5">
                <p>© 2023 Все права защищены</p>
            </footer>
        </div>
    );
}

export default AdminPanel;
