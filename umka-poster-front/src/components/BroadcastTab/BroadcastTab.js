import React, { useEffect, useRef, useState, useContext } from 'react';
import Quill from 'quill';
import EventContext from '../../EventContext';
import 'quill-emoji/dist/quill-emoji.js';
import { openSSE, fetchAPI } from '../../api';



const BroadcastTab = ({ isActive, setStatistics }) => {
    console.log("BroadcastTab render. Is active:", isActive); // Логирование активности вкладки

    const { setEvents, clearEvents } = useContext(EventContext);

    const [eventSource, setEventSource] = useState(null);

    const initStream = async (messageDelta, queryParams) => {
        try {
            const queryParamsStr = new URLSearchParams(queryParams).toString();
            console.log('Init stream:', { delta: messageDelta, queryParamsStr });

            const response = await fetchAPI(`/init_message_event_stream?${queryParamsStr}`, {
                method: 'POST',
                headers: {
                'Content-Type': 'application/json'
                },
                body: JSON.stringify(messageDelta)
            });
            const data = await response.json();
            return data.session_id;
        } catch (error) {
            console.error("Error during stream initialization:", error);
            return null;
        }
    };

    const handleButtonClick = async (e) => {
        e.preventDefault();
        clearEvents();
      
        const sendButton = e.target;
        sendButton.disabled = true;
        sendButton.textContent = "Отправка...";
        document.getElementById('summary').classList.add('d-none');
      
        const photoId = document.getElementById('photoId').value;
        const isTest = document.getElementById('testBroadcast').value === "yes";
        const testUserId = document.getElementById('testUser').value;
        const isLocal = document.getElementById('imageLoadSettings').value === "yes";
      
        const queryParams = {
          is_test: isTest,
          is_local: isLocal,
          image_path: photoId,
          test_user_id: testUserId
        };
      
        const messageDelta = quillRef.current.getContents();

        // Инициализация стрима и получение session_id
        const sessionId = await initStream(messageDelta, queryParams);
      
        if (sessionId && !eventSource) {
          const handlers = {
            update: (newEvent) => {
                console.log("Update");
                console.log("New event:", newEvent);
                setEvents(prevEvents => [...prevEvents, newEvent]);
            },
            completed: (statistics) => {
                setEventSource(null);
                console.log("Statistics:", statistics);
                setStatistics(statistics);
                sendButton.disabled = false;
                sendButton.textContent = "Запустить";
                document.querySelector('.nav-tabs a[href="#journal"]').click();
                document.getElementById('summary').classList.remove('d-none');
            },
            error: (error) => {
                setEventSource(null);
                sendButton.disabled = false;
                sendButton.textContent = "Запустить";
            }
          };
          // Открываем SSE-соединение с session_id
          const newEventSource = openSSE("/stream_message_events", handlers, { session_id: sessionId });
          setEventSource(newEventSource);
        }
    };
    
    const quillRef = useRef(null);

    useEffect(() => {
        if (!quillRef.current) {
            const toolbarOptions = {
                container: [
                    ['bold', 'italic'],
                ],
                handlers: { 'emoji': function() {} }
            };

            // Если захочется оформлять сообщения с emoji в будущем
            quillRef.current = new Quill('#editor', {
                theme: 'snow',
                modules: {
                    toolbar: toolbarOptions,
                    "emoji-toolbar": false,
                    "emoji-textarea": false,
                    "emoji-shortname": false,
                }
            });
        }
    }, []);

    return (
        <div className={`tab-pane ${isActive ? 'active' : ''}`} id="broadcast">
            <form>
                <div className="form-group">
                    <label for="messageText">Текст сообщения</label>
                    <div id="editor" style={{height: '150px'}}></div>
                </div>
                <div className="form-group">
                    <label for="photoId">Изображение</label>
                    <input type="text"  className="form-control" id="photoId" />
                </div>
                <button type="submit" className="btn btn-primary" id="sendButton" onClick={handleButtonClick}>Отправить</button>
            </form>
        </div>
    );
}

export default BroadcastTab;