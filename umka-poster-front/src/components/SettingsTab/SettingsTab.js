import React from 'react';
import './SettingsTab.css';


const SettingsTab = ({ isActive, user }) => {

    console.log("SettingsTab render. Is active:", isActive); // Логирование активности вкладки

    return (
        <div className={`tab-pane ${isActive ? 'active' : ''}`} id="settings">
            <form>
                <div className="form-group">
                    <label for="testBroadcast">Тестовая рассылка</label>
                    <select className="form-control settings-input" id="testBroadcast" defaultValue="yes">
                        <option value="yes">Да</option>
                        <option value="no">Нет</option>
                    </select>
                </div>
                <div className="form-group">
                    <label for="testUser">Тестовый пользователь</label>
                    <input type="text" className="form-control settings-input" id="testUser" defaultValue={user.user_id} disabled />
                </div>
                {/* <button type="submit" className="btn btn-primary">Применить</button> */}
                <div className="form-group">
                    <label for="imageLoadSettings">Источник изображения</label>
                    <select className="form-control settings-input" id="imageLoadSettings" defaultValue="no" disabled>
                        <option value="yes">Локально</option>
                        <option value="no">S3</option>
                    </select>
                </div>
            </form>
        </div>
    );
}

export default SettingsTab;
