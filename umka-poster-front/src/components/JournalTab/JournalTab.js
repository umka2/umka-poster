import {React, useContext, useEffect, useState } from 'react';
import { formatTime } from '../../utils';
import EventContext from '../../EventContext';
import Pagination from 'react-bootstrap/Pagination';



const ITEMS_PER_PAGE = 50; // Количество элементов на странице


const Summary = ({ statistics }) => {
    if (!statistics) return null;

    return (
        <>
            <div>
                <strong>Итоги:</strong>
                <span className="badge badge-time">Время: {formatTime(statistics.elapsed_time)}</span>
            </div>
            <div className="mt-2">
                <span className="badge badge-success">Успешно: {statistics.success_count}</span>
                <span className="badge badge-warning">Отписки: {statistics.unsubscribed_count}</span>
                <span className="badge badge-danger">Ошибки: {statistics.failure_count}</span>
            </div>
        </>
    );
}

const JournalTab = ({ isActive, statistics }) => {

    const { events } = useContext(EventContext);
    const [pagedEvents, setPagedEvents] = useState([]);
    const [currentPage, setCurrentPage] = useState(1);
    const [totalPages, setTotalPages] = useState(1);

    useEffect(() => {
        const startIndex = (currentPage - 1) * ITEMS_PER_PAGE;
        const endIndex = startIndex + ITEMS_PER_PAGE;
        setPagedEvents(events.slice(startIndex, endIndex));
    }, [events, currentPage]);

    useEffect(() => {
        setTotalPages(Math.ceil(events.length / ITEMS_PER_PAGE));
    }, [events.length]);


    const VISIBLE_PAGES = 5; // Число видимых страниц

    let startPage = 1;
    let endPage = totalPages;

    if (totalPages > VISIBLE_PAGES) {
        // Всегда отображаем 2 страницы до и 2 после текущей
        let maxPagesBeforeCurrentPage = Math.floor(VISIBLE_PAGES / 2);
        let maxPagesAfterCurrentPage = Math.ceil(VISIBLE_PAGES / 2) - 1;

        if (currentPage <= maxPagesBeforeCurrentPage) {
            // Текущая страница ближе к началу
            endPage = VISIBLE_PAGES;
        } else if (currentPage + maxPagesAfterCurrentPage >= totalPages) {
            // Текущая страница ближе к концу
            startPage = totalPages - VISIBLE_PAGES + 1;
        } else {
            // Текущая страница где-то посередине
            startPage = currentPage - maxPagesBeforeCurrentPage;
            endPage = currentPage + maxPagesAfterCurrentPage;
        }
    }

    console.log("JournalTab render. Is active:", isActive, "Statistics:", statistics);

    return (
        <div className={`tab-pane ${isActive ? 'active' : ''}`} id="journal">
            <table className="table" id="journalTable">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>Время</th>
                        <th>ID пользователя</th>
                        <th>Статус</th>
                    </tr>
                </thead>
                <tbody>
                    {pagedEvents.map((event, index) => (
                        <tr key={index}>
                            <td>{(currentPage - 1) * ITEMS_PER_PAGE + (index + 1)}</td> 
                            <td>{event.date}</td>
                            <td>{event.user_id}</td>
                            <td>{event.status}</td>
                        </tr>
                    ))}
                </tbody>
            </table>
            <div className='pagination-right mt-3'>
                <Pagination>
                    <Pagination.First onClick={() => setCurrentPage(1)} />
                    <Pagination.Prev onClick={() => setCurrentPage(Math.max(currentPage - 1, 1))} disabled={currentPage === 1} />
                    {[...Array((endPage + 1) - startPage).keys()].map(page => (
                        <Pagination.Item key={startPage + page} active={startPage + page === currentPage} onClick={() => setCurrentPage(startPage + page)}>
                            {startPage + page}
                        </Pagination.Item>
                    ))}
                    <Pagination.Next onClick={() => setCurrentPage(Math.min(currentPage + 1, totalPages))} disabled={currentPage === totalPages} />
                    <Pagination.Last onClick={() => setCurrentPage(totalPages)} />
                </Pagination>
            </div>
            <div className="mt-3" id="summary">
                <Summary statistics={statistics} />
            </div>
        </div>

    );
}

export default JournalTab;
