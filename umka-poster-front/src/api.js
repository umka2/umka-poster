// api.js

console.log(`REACT_APP_BACKEND_URL: ${process.env.REACT_APP_BACKEND_URL}`);

const BASE_URL = process.env.REACT_APP_BACKEND_URL || 'http://localhost:8000'

console.log(`API Base URL: ${BASE_URL}`);

export async function fetchAPI(endpoint, options = {}) {
    console.log(`Making request to: ${BASE_URL}${endpoint}`);
    const response = await fetch(`${BASE_URL}${endpoint}`, options);
    if (!response.ok) {
        throw new Error(`Failed to fetch ${endpoint}: ${response.statusText}`);
    }
    return response;
}

export function openSSE(endpoint, handlers = {}, queryParams = {}, payload = null) {
    const url = new URL(`${BASE_URL}${endpoint}`);
    Object.keys(queryParams).forEach(key => url.searchParams.append(key, queryParams[key]));

    const eventSourceInitDict = payload ? { method: 'POST', body: JSON.stringify(payload) } : {};
    const eventSource = new EventSource(url, eventSourceInitDict);

    if (handlers.update) {
        eventSource.addEventListener("update", (event) => {
            const data = JSON.parse(event.data);
            handlers.update(data);
        });
    }
  
    if (handlers.completed) {
        eventSource.addEventListener("completed", (event) => {
            const data = JSON.parse(event.data);
            handlers.completed(data);
        });
    }
  
    if (handlers.error) {
        eventSource.addEventListener("error", (event) => {
            console.error("SSE Connection Error:", event);
            eventSource.close();  // Закрытие соединения
            handlers.error(event);
        });
    }
    
    return eventSource;
}
