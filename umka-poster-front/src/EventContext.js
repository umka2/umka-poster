import React, { useState } from 'react';

const EventContext = React.createContext();

export const EventProvider = ({ children }) => {
  const [events, setEvents] = useState([]);

  const clearEvents = () => {
    setEvents([]);
  };

  return (
    <EventContext.Provider value={{ events, setEvents, clearEvents }}>
      {children}
    </EventContext.Provider>
  );
};

export default EventContext;
