import json
from typing import Any, Dict, List

from pydantic import BaseModel, Field
from datetime import datetime


class AuthRequest(BaseModel):
    login: str
    password: str


class DeltaOp(BaseModel):
    insert: Any
    attributes: Dict[str, Any] = Field(default={})


class Delta(BaseModel):
    ops: List[DeltaOp]


class Statistics(BaseModel):
    success_count: int
    unsubscribed_count: int
    failure_count: int
    elapsed_time: float

    def to_dict(self):
        return self.dict()

    def to_json(self):
        return json.dumps(self.to_dict())


class Message(BaseModel):
    user_id: str
    status: str = "Запланировано"
    date: str = datetime.now().strftime('%Y-%m-%d %H:%M:%S')

    def _update_time(self):
        self.date = datetime.now().strftime('%Y-%m-%d %H:%M:%S')

    @staticmethod
    def create_new(user_id):
        return Message(user_id=user_id)

    def set_status_success(self):
        self.status = "Успешно"
        self._update_time()
        return self

    def set_status_unsubscribed(self):
        self.status = "Заблокировано"
        self._update_time()
        return self

    def set_status_error(self):
        self.status = "Ошибка"
        self._update_time()
        return self

    def to_dict(self):
        return self.dict()

    def to_json(self):
        return json.dumps(self.to_dict())
