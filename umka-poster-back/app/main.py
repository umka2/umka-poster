from fastapi import FastAPI, Query, HTTPException
from fastapi.responses import JSONResponse
from starlette.middleware import Middleware
from starlette.middleware.cors import CORSMiddleware
from starlette.responses import StreamingResponse

from app.model import AuthRequest, Delta
from app.service import AuthService, BroadcastStatisticsService, MessageService

from uuid import uuid4
import logging

logging.basicConfig(level=logging.INFO)

origins = [
    "http://localhost:3000",
    "http://127.0.0.1:3000",
    "http://3.106.30.233:3000"
]

middleware = [
    Middleware(
        CORSMiddleware,
        allow_origins=origins,
        allow_credentials=True,
        allow_methods=['*'],
        allow_headers=['*'],
        expose_headers=["*"],
        max_age=600
    )
]

app = FastAPI(middleware=middleware)
sessions = {}

broadcast_statistics_service = BroadcastStatisticsService()
auth_service = AuthService()
message_service = MessageService()


@app.post("/authenticate")
def authenticate(auth_request: AuthRequest):
    user = auth_service.authenticate(auth_request.login, auth_request.password)
    if user:
        return {"message": "Authentication successful", "user": user}
    else:
        raise HTTPException(status_code=401, detail="Authentication failed")


@app.get("/get_broadcast_statistics")
def get_broadcast_statistics(page: int = Query(1), items_per_page: int = Query(10)):
    statistics_data = broadcast_statistics_service.get_broadcast_statistics(page, items_per_page)
    if statistics_data:
        return statistics_data
    else:
        raise HTTPException(status_code=400, detail="Failed to fetch statistics")


@app.post('/init_message_event_stream')
async def init_stream(delta: Delta,
                      is_test: bool = Query(True, alias="is_test"),
                      is_local: bool = Query(False, alias="is_local"),
                      image_path: str = Query(None, alias="image_path"),
                      test_user_id: str = Query(None, alias="test_user_id")):
    session_id = str(uuid4())
    sessions[session_id] = {
        "delta": delta,
        "is_test": is_test,
        "is_local": is_local,
        "image_path": image_path,
        "test_user_id": test_user_id
    }
    return {"session_id": session_id}


@app.get('/stream_message_events')
async def stream_message_events(session_id: str):
    try:
        if session_id not in sessions:
            return JSONResponse(content={"message": "Invalid session_id"}, status_code=400)

        params = sessions[session_id]
        delta = params["delta"]
        query_params = (params["is_test"], params["is_local"], params["image_path"], params["test_user_id"])

        return StreamingResponse(message_service.stream_message_events(delta.dict(), query_params),
                                 media_type="text/event-stream; charset=utf-8")
    except Exception as e:
        return JSONResponse(content={"message": f"An error occurred: {e}"}, status_code=500)


@app.post("/publish_messages_direct")
async def publish_messages_direct(
        delta: Delta,
        is_test: bool = Query(True, alias="is_test"),
        is_local: bool = Query(False, alias="is_local"),
        image_path: str = Query(None, alias="image_path"),
        test_user_id: str = Query(None, alias="test_user_id")
):
    try:
        query_params = (is_test, is_local, image_path, test_user_id)

        results = await message_service.publish_messages_directly(delta.dict(), query_params)

        return JSONResponse(content={"status": "success", "results": results})

    except Exception as e:
        return JSONResponse(content={"message": f"An error occurred: {e}"}, status_code=500)