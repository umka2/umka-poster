import json
import os
import boto3
import logging
import aioboto3
import asyncio
import aiofiles

from telegram import InputFile, Bot, error
from telegram.constants import ParseMode

from app.dao import BroadcastStatisticsDao, UserAnalyticsDao, BlockedUserDao
from app.dao import PrivilegedUserDao
from datetime import datetime
from queue import Queue
from dotenv import load_dotenv

from app.model import Statistics, Message

load_dotenv()

UMKA_BOT_TOKEN = os.environ.get('UMKA_BOT_TOKEN', '123456:default_token')
MAX_RETRIES = int(os.environ.get('MAX_RETRIES', 5))
DELAY = int(os.environ.get('DELAY', 5))
BUCKET_NAME = os.environ.get('BUCKET_NAME', 'default_bucket')
UMKA_BOT_ID = UMKA_BOT_TOKEN.split(':')[0]

s3 = boto3.resource('s3')


class AuthService:
    def __init__(self):
        self.dao = PrivilegedUserDao()

    def authenticate(self, login: str, password: str):
        try:
            user = self.dao.authenticate_user(login, password)
            if user:
                user.pop('user_pass', None)
                return user
            else:
                return None
        except Exception as e:
            logging.error(f"Error during authentication: {e}")
            return None


class BroadcastStatisticsService:
    def __init__(self):
        self.dao = BroadcastStatisticsDao()

    def get_broadcast_statistics(self, page: int, items_per_page: int):
        try:
            statistics_data = self.dao.get_all_statistics(UMKA_BOT_ID, page, items_per_page)
            return statistics_data
        except Exception as e:
            logging.error(f"Error during fetching broadcast statistics: {e}")
            return None


class MessageService:
    def __init__(self):
        self.message_queue = Queue()
        self.user_analytics_dao = UserAnalyticsDao()
        self.broadcast_statistics_dao = BroadcastStatisticsDao()
        self.blocked_user_dao = BlockedUserDao()
        self.umka_bot = Bot(token=UMKA_BOT_TOKEN)

    @staticmethod
    def extract_final_stats(events):
        if not events:
            return None

        last_event = events[-1]
        json_str = last_event.split('data: ')[1].strip()  # извлечение части, содержащей JSON
        return json.loads(json_str)

    async def publish_messages_directly(self, data, query_params):
        results = []
        async for message in self.stream_message_events(data, query_params):
            results.append(message)
        return self.extract_final_stats(results)

    async def stream_message_events(self, data, query_params):
        is_test, is_local, image_path, test_user_id = query_params
        logging.debug(
            f"Parsed query params: is_test={is_test}, is_local={is_local}, image_path={image_path}, test_user_id={test_user_id}")

        text, md_text = self._process_data(data)

        user_ids_fut = self._get_user_ids(is_test, test_user_id)
        blocked_user_ids_fut = self._get_blocked_users()
        temp_path_future = self._async_download_image(image_path)

        user_ids, blocked_user_ids, temp_path = await asyncio.gather(user_ids_fut,
                                                                     blocked_user_ids_fut, temp_path_future)

        filtered_user_ids = self._filter_blocked_users(user_ids, blocked_user_ids)

        async for message in self._send_messages(filtered_user_ids, temp_path, md_text, text, image_path, is_test):
            yield message

    @staticmethod
    def _process_data(data):
        logging.debug(f"Received data for processing: {data}")

        def validate_caption(caption):
            # Max symbol count in Telegram - 4096
            if len(caption) > 4096:
                logging.error("Caption is too long. The maximum length is 4096 characters.")
                raise Exception("Caption is too long. The maximum length is 4096 characters.")

        text = ''.join(op.get('insert', '') for op in data.get('ops', []))
        logging.debug(f"Joined text: {text}")

        validate_caption(text)

        delta = data.get('ops', [])
        logging.debug(f"Delta ops: {delta}")

        def delta_to_markdown(ops):
            markdown = ""
            for op in ops:
                text_part = op['insert']
                attrs = op.get('attributes', {})
                # Check both args this is not supported in Markdown
                if 'bold' in attrs and 'italic' in attrs:
                    markdown += f"*{text_part}*"
                elif 'bold' in attrs:
                    markdown += f"*{text_part}*"
                elif 'italic' in attrs:
                    markdown += f"_{text_part}_"
                else:
                    markdown += text_part
            return markdown

        text_markdown = delta_to_markdown(delta)
        logging.info(f"Converted to markdown: {text_markdown}")
        return text, text_markdown

    @staticmethod
    async def _async_download_image(image_key):
        session = aioboto3.Session()
        async with session.resource('s3') as s3_resource:
            bucket = await s3_resource.Bucket(BUCKET_NAME)
            await bucket.download_file(image_key, "/tmp/image.jpg")
        return "/tmp/image.jpg"

    @staticmethod
    def _download_image(image_key):
        bucket = s3.Bucket(BUCKET_NAME)
        bucket.download_file(image_key, "/tmp/image.jpg")
        return "/tmp/image.jpg"

    @staticmethod
    def _filter_blocked_users(user_ids, blocked_user_ids):
        return [user for user in user_ids if user not in set(blocked_user_ids)]

    async def _get_user_ids(self, is_test, test_user_id):
        if is_test:
            return [test_user_id] * 10
        else:
            return await self.user_analytics_dao.get_all_users()

    async def _get_blocked_users(self):
        return await self.blocked_user_dao.get_all_users_async()

    async def _send_messages(self, user_ids, temp_path, converted_md, text, image_key, is_test):
        logging.info(f"Sending messages to {len(user_ids)} users")

        success_count = 0
        unsubscribed_count = 0
        failure_count = 0

        start_time = datetime.now()

        for user_id in user_ids:
            retries = 0
            success = False
            forbidden = False
            message = Message.create_new(user_id)

            while retries < MAX_RETRIES and not success and not forbidden:
                try:
                    async with aiofiles.open(temp_path, 'rb') as photo:
                        await photo.seek(0)
                        byte_content = await photo.read()

                        await self.umka_bot.send_photo(user_id, InputFile(byte_content), caption=converted_md,
                                                       parse_mode=ParseMode.MARKDOWN)

                        await asyncio.sleep(DELAY)  # e.retry_after

                        success_count += 1
                        success = True

                        message.set_status_success()
                        yield f"event: update\ndata: {message.to_json()}\n\n"

                        logging.info(f"Message sent successfully to user {user_id}")
                        break
                except error.Forbidden:
                    await self.blocked_user_dao.add_user(user_id)
                    unsubscribed_count += 1
                    forbidden = True

                    message.set_status_unsubscribed()
                    yield f"event: update\ndata: {message.to_json()}\n\n"

                    logging.warning(f"Cannot send message: User {user_id} has blocked the bot.")
                    break
                except error.RetryAfter as e:
                    await asyncio.sleep(DELAY)  # e.retry_after
                    retries += 1
                    logging.error(f"Error when sending message: {e}")
                except Exception as e:
                    logging.error(f"Unexpected error occurred: {e}")
                    retries += 1

            if not success and not forbidden:  # If after all retries the message was not sent
                failure_count += 1
                message.set_status_error()
                yield f"event: update\ndata: {message.to_json()}\n\n"
                logging.error(f"Failed to send message to user {user_id} after {MAX_RETRIES} retries.")

        elapsed_time = (datetime.now() - start_time).total_seconds()

        stats = Statistics(
            success_count=success_count,
            unsubscribed_count=unsubscribed_count,
            failure_count=failure_count,
            elapsed_time=elapsed_time
        )

        await self._save_statistics(stats.to_dict(), text, image_key, is_test)

        yield f"event: completed\ndata: {stats.to_json()}\n\n"

    async def _save_statistics(self, statistics, caption, image_path, is_test):
        broadcast_type = "test" if is_test else "subscribers"
        await self.broadcast_statistics_dao.add_statistics_async(UMKA_BOT_ID, statistics, caption, image_path,
                                                                 broadcast_type)
