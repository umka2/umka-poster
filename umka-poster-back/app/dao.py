import logging
from datetime import datetime, timedelta

import boto3 as boto3
from aioboto3 import Session
from botocore.exceptions import ClientError
from dateutil.parser import parse

from functools import wraps
from decimal import Decimal

import time


def measure_time(func):
    @wraps(func)
    def wrapper(*args, **kwargs):
        start_time = time.time()
        result = func(*args, **kwargs)
        end_time = time.time()
        execution_time = end_time - start_time
        logging.info(f"Execution time of {func.__name__}: {execution_time} s")
        return result

    return wrapper


class PrivilegedUserDao:
    def __init__(self):
        self.session = Session()
        self.dynamodb = boto3.resource('dynamodb')
        self.table = self.dynamodb.Table('privileged_users')

    def _get_user_by_login(self, login):
        try:
            response = self.table.query(
                IndexName='login-index',
                KeyConditionExpression='user_login = :login',
                ExpressionAttributeValues={
                    ':login': login
                }
            )
            items = response['Items']
            logging.info(f"Retrieved users by login: {items}")
            return items
        except Exception as e:
            logging.error(f"Failed to get user by login from DynamoDB: {e}")
            return []

    @measure_time
    def authenticate_user(self, login, password):
        try:
            users = self._get_user_by_login(login)
            if not users:
                logging.warning(f"No users found for login: {login}")
                return None
            user = users[0]  # Assuming each login is unique
            if user['user_pass'] == password:
                logging.info(f"User {login} authenticated successfully.")
                return user
            else:
                logging.warning(f"Password mismatch for user: {login}")
                return None
        except Exception as e:
            logging.error(f"Error during authentication: {e}")
            return None


class BroadcastStatisticsDao:
    def __init__(self):
        self.session = Session()
        self.dynamodb = boto3.resource('dynamodb')
        self.table = self.dynamodb.Table('broadcast_statistics')

    async def add_statistics_async(self, bot_id, statistics, message_text, image_id, broadcast_type):
        try:
            async with self.session.resource('dynamodb') as dynamo_resource:
                table = await dynamo_resource.Table("broadcast_statistics")
                timestamp = datetime.utcnow().strftime('%Y-%m-%d %H:%M:%S')

                await table.put_item(
                    Item={
                        'bot_id': bot_id,
                        'timestamp': timestamp,
                        'message_text': message_text,
                        'image_id': image_id,
                        'success_count': int(statistics['success_count']),
                        'unsubscribed_count': int(statistics['unsubscribed_count']),
                        'failure_count': int(statistics['failure_count']),
                        'elapsed_time': Decimal(str(statistics['elapsed_time'])),
                        'broadcast_type': broadcast_type
                    }
                )
                logging.info(f"Successfully added statistics to DynamoDB.")
        except Exception as e:
            logging.error(f"Failed to add statistics to DynamoDB: {e}")

    @measure_time
    def get_total_records(self):
        try:
            response = self.table.scan(Select='COUNT')
            return response['Count']
        except Exception as e:
            logging.error(f"Failed to get total count from DynamoDB: {e}")
            return 0

    @measure_time
    def get_all_statistics(self, bot_id, page=1, items_per_page=10):
        try:
            start_key = None
            if page > 1:
                # If this is not the first page, calculate ExclusiveStartKey
                start_key = {'bot_id': bot_id, 'timestamp': self._get_start_key_for_page(page, items_per_page)}

            if start_key:
                response = self.table.scan(Limit=items_per_page, ExclusiveStartKey=start_key)
            else:
                response = self.table.scan(Limit=items_per_page)

            total_records = self.get_total_records()
            items = response['Items']

            # Converting decimal fractions back to integers
            for item in items:
                item['success_count'] = int(item['success_count'])
                item['unsubscribed_count'] = int(item['unsubscribed_count'])
                item['failure_count'] = int(item['failure_count'])

            return {
                'items': items,
                'totalRecords': total_records,
                'itemsPerPage': items_per_page
            }
        except Exception as e:
            logging.error(f"Failed to get statistics from DynamoDB: {e}")
            return {
                'items': [],
                'totalRecords': 0,
                'itemsPerPage': items_per_page
            }

    def _get_start_key_for_page(self, page, items_per_page):
        # This method returns the start key for the given page
        offset = (page - 1) * items_per_page
        response = self.table.scan(Limit=offset)
        return response['Items'][-1]['timestamp']


class BlockedUserDao:  # Users who have unsubscribed from the mailing list
    def __init__(self):
        self.session = Session()
        self.dynamodb = boto3.resource('dynamodb')
        self.table = self.dynamodb.Table('blocked_users')

    async def add_user(self, user_id):
        try:
            async with self.session.resource('dynamodb') as dynamo_resource:
                table = await dynamo_resource.Table('blocked_users')
                await table.put_item(
                    Item={
                        'user_id': str(user_id),
                    }
                )
        except ClientError as e:
            logging.error(e)

    async def get_all_users_async(self):
        try:
            async with self.session.resource('dynamodb') as dynamo_resource:
                table = await dynamo_resource.Table('blocked_users')
                response = await table.scan()
                users = [int(item['user_id']) for item in response['Items']]
                return users
        except ClientError as e:
            logging.error(e)
            return None


class UserAnalyticsDao:
    def __init__(self):
        self.session = Session()
        self.dynamodb = boto3.resource('dynamodb')
        self.table = self.dynamodb.Table("user_analytics")

    @measure_time
    def get_all_inactive_users(self, days):  # Returns users who do not use Umka
        now = datetime.now()
        cutoff = now - timedelta(days=days)
        try:
            response = self.table.scan()  # todo: slow
            items = []
            for item in response['Items']:
                last_seen = parse(item['last_seen'])
                if last_seen < cutoff:
                    items.append(int(item['user_id']))
            return items
        except ClientError as e:
            logging.error(e)

    async def get_all_users(self):  # Returns all users who have used Umka at least once
        try:
            async with self.session.resource('dynamodb') as dynamo_resource:
                table = await dynamo_resource.Table('user_analytics')
                response = await table.scan()
                users = [int(item['user_id']) for item in response['Items']]
                return users
        except ClientError as e:
            logging.error(e)
            return None

    @measure_time
    def get_inactive_users_count(self, days=30):  # Returns the number of users who do not use Umka
        now = datetime.now()
        cutoff = now - timedelta(days=days)

        response = self.table.scan()  # todo: slow

        active_users_count = 0
        for item in response['Items']:
            last_seen = parse(item['last_seen'])
            if last_seen < cutoff:
                active_users_count += 1

        return active_users_count
