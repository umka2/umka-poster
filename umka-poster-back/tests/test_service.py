import unittest
from app.service import AuthService, MessageService
from unittest.mock import patch, Mock
from app.service import BroadcastStatisticsService
from unittest.mock import ANY


class TestAuthService(unittest.TestCase):
    @patch('app.service.PrivilegedUserDao.authenticate_user')
    def test_authenticate_success(self, mock_authenticate_user):
        mock_authenticate_user.return_value = {
            'user_id': '435461305',
            'user_login': 'beskrovnykh',
            'user_pass': 'andreyqwe',
            'user_role': 'ADMIN',
            'bot_id': '6211631999'
        }
        service = AuthService()
        result = service.authenticate('beskrovnykh', 'andreyqwe')

        expected_output = {
            "user_id": "435461305",
            "user_login": "beskrovnykh",
            "user_role": "ADMIN",
            "bot_id": "6211631999"
        }

        self.assertEqual(result, expected_output)

    @patch('app.service.PrivilegedUserDao.authenticate_user')
    def test_authenticate_failure(self, mock_authenticate_user):
        mock_authenticate_user.return_value = None
        service = AuthService()
        result = service.authenticate('beskrovnykh', 'wrong_pass')
        self.assertEqual(result, None)


class TestBroadcastStatisticsService(unittest.TestCase):
    @patch('app.service.BroadcastStatisticsDao.get_all_statistics')  # Mocking as it is imported in the service
    def test_get_broadcast_statistics(self, mock_get_all_statistics):
        mock_bot_id = "6211631999"
        mock_data = {
            "items": [
                {
                    "image_id": "nature/20141226-IMG_8034.jpg",
                    "bot_id": mock_bot_id,
                    "timestamp": "2023-09-07 08:21:00",
                    "failure_count": 0,
                    "elapsed_time": 3.2627439498901367,
                    "broadcast_type": "test",
                    "success_count": 1,
                    "message_text": "Тестовое сообщеине",
                    "unsubscribed_count": 0
                }
            ],
            "totalRecords": 3,
            "itemsPerPage": 2
        }

        mock_get_all_statistics.return_value = mock_data

        service = BroadcastStatisticsService()
        result = service.get_broadcast_statistics(2, 2)

        self.assertEqual(result, mock_data)


class TestMessageService(unittest.TestCase):

    @patch('app.service.Bot')  # Replace with the actual import path
    @patch('app.service.UserAnalyticsDao')  # Replace with the actual import path
    @patch('app.service.BlockedUserDao')  # Replace with the actual import path
    @patch('app.service.s3.Bucket')  # Replace with the actual import path
    @unittest.skip("Skipping this test for now")
    def test_send_message_events(self, MockedBucket, MockedBlockedUserDao, MockedUserAnalyticsDao, MockedBot):
        # Create a mock instance for Bot
        mock_bot_instance = Mock()
        MockedBot.return_value = mock_bot_instance
        mock_bot_instance.send_photo.return_value = True

        MockedUserAnalyticsDao().get_all_users.return_value = ['test_user']
        MockedBlockedUserDao().get_all_users.return_value = []
        MockedBucket().download_file.return_value = None

        # Initialize your class
        service = MessageService()

        # Test data and query params
        data = {
            "ops": [
                {
                    "insert": "Hello, "
                },
                {
                    "insert": "world"
                },
                {
                    "insert": "!\n"
                }
            ]
        }
        query_params = (True, False, 'some/image/path', 'test_user')

        # Call the method
        statistics = service.send_message_events(data, query_params)

        # Assertions
        self.assertEqual(statistics['success_count'], 1)
        self.assertEqual(statistics['unsubscribed_count'], 0)
        self.assertEqual(statistics['failure_count'], 0)

        # Verify that send_photo was called on the mock Bot instance
        mock_bot_instance.send_photo.assert_called_with(
            'test_user',
            ANY,  # You can replace this with the expected InputFile if you know it
            caption="Hello, world!\n",
            parse_mode=ANY  # Replace with the expected ParseMode
        )


if __name__ == '__main__':
    unittest.main()
